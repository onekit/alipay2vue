import x2x from '../alipay2vue/index.js';
import '../public/onekit/onekit.css';
import './app.css';
import Vue from 'vue';
const { pages, js } = x2x;
Vue.use(x2x);
import Router from 'vue-router';
Vue.use(Router);
import $ from 'jquery'
import {URL,DEVICE} from 'oneutil'
const { tabs, activity, previewMedia, openLocation } = pages;
import APP_JSON from './app.json.js'

import THEMES_JSON from './themes.json.js'
window.THEMES_JSON = THEMES_JSON
import PROJECT_JSON from './project.config.json.js'
import ActionPannel from "vue-action-pannel"
const ENTRY = window.ENTRY = new URL(location.href)
////////
Vue.use(ActionPannel)


window.eventChannels = {}
window.APP_JSON = APP_JSON //window.APP_JSON = APP_JSON;
window.TEMP = {}
window.axios_CONFIG = {}
window.FSO = {}
////////////////////////////////////////////////

function onekit_onError (error) {
  if (window.onError) {
    window.onError(error)
  } else {
    throw error
  }
}
Vue.config.errorHandler = onekit_onError

if (window.onThemeChange) {
  Object.defineProperty(window.THEME, 'theme', {
    get: function () {
      return window.THEME;
    },

    set: function (newValue) {
      window.onThemeChange(newValue)
    }
  })
}
window.addEventListener('unhandledrejection', function () {
  if (window.onUnhandledRejection) {
    window.addEventListener('unhandledrejection', vue_e => {
      const wx_reason = vue_e.reason
      const wx_promise = vue_e.promise
      window.onUnhandledRejection(wx_reason, wx_promise);
    })
  }
})
document.addEventListener("visibilitychange", function () {

  if (document.hidden) {
    if (window.onAudioInterruptionBegin) {
      window.onAudioInterruptionBegin()
    }
    if (window.onAppHide) {
      const wx_path = js.OneKit.current().$route.path
      const wx_query = {
        params: "",
        query: ""
      }
      const wx_scene = undefined;
      const wx_referrerInfo = {};
      let wx_res = {
        path: wx_path,
        scene: wx_scene,
        query: wx_query,
        referrerInfo: wx_referrerInfo,
        shareTicket: undefined
      };
      window.onAppHide(wx_res)
    }

  } else {
    if (window.onAppShow) {
      const wx_path = js.OneKit.current().$route.path
      const wx_query = {
        params: "",
        query: ""
      }
      const wx_scene = undefined;
      const wx_referrerInfo = {};
      let wx_res = {
        path: wx_path,
        scene: wx_scene,
        query: wx_query,
        referrerInfo: wx_referrerInfo,
        shareTicket: undefined
      };
      window.onAppShow(wx_res)

    }
    if (window.onAudioInterruptionEnd) {
      window.onAudioInterruptionEnd()
    }

  }
})



// if(window.offHeadersReceived) {
//   console.log(window.onHeadersReceived)
// }

// if(window.onHeadersReceived){
//   window.onHeadersReceived()
// }




//////////////////////////
const screen_width = DEVICE.isMobile() ? (($(window).width()) - 0) : 750;
$("body").css('--screen-width', screen_width + "px");
$('body').css('height', 100 + 'vh')
//
let router = {
  mode: 'history',
  routes: [{
    path: '/',
    redirect: `/${APP_JSON.pages[0]}`,
  }, {
    path: '/onekit/previewMedia',
    component: previewMedia,
  },
  {
    path: '/onekit/openLocation',
    component: openLocation,
  }
  ]
};
//
let tabPages = [];
let tabBar = APP_JSON["tabBar"];
let entry
if (tabBar) {
  entry = tabBar.list[0].pagePath
  //
  let children = [];
  for (let tab of tabBar.list) {
    let pagePath = tab["pagePath"];

    children.push({
      path: `/${pagePath}`,
      component: () => import(`@/${pagePath}.vue`),
      data () {
        return {}
      }
    });
    tabPages.push(pagePath);
  }
  router.routes[0].component = tabs;
  router.routes[0].children = children;
} else {
  entry = APP_JSON.pages[0]
}
//////////////
let PAGES = APP_JSON.pages;
const subpackages = APP_JSON.subpackages || APP_JSON.subPackages
if (subpackages) {
  for (const subpackage of subpackages)
    PAGES = PAGES.concat(subpackage.pages.map((page) => {
      return `${subpackage.root}/${page}`
    }))
}
if (PAGES.includes(ENTRY.path.substr(1))) {
  entry = ENTRY.path.substr(1)
}
//
for (let page of PAGES) {
  if (tabBar && tabPages.indexOf(page) >= 0) {
    continue;
  }
  router.routes.push({
    path: `/${page}`,
    component: () => import(`@/${page}.vue`)
  });
}

////////////////////////////

const wx_path = entry
const wx_query = {
  params: "",
  query: ""
}
const wx_scene = 1001;
const wx_referrerInfo = {
  appId: PROJECT_JSON.appid,
  extraData: {}
};
let wx_option = {
  path: wx_path,
  scene: wx_scene,
  query: wx_query,
  referrerInfo: wx_referrerInfo,
  shareTicket: {}
};
window.OPTION = wx_option
////////////////////////////
import './app_.js';

if (!APP_JSON.pages.includes(ENTRY.path.substr(1))) {
  if (window.onPageNotFound) {
    const wx_res = {}
    window.onPageNotFound(wx_res);
  }
}
//////////////////////////////

const authSetting = { scope_userLocation: true, scope_userInfo: true, scope_address: true, scope_invoice: true, scope_invoiceTitle: true }

localStorage.setItem('authSetting', JSON.stringify(authSetting))

document.body.style.height = "100%";
Vue.use(x2x);

export default {
  router: new Router(router),
  render: h => h(activity)
}