/* eslint-disable */
import my from '../../alipay2vue/my.js';
import macro from '../../alipay2vue/macro.js';
const {getApp,getCurrentPages} = macro;
export default function debounce(fn,wait){
  var timeout;
  return function(){
    var ctx = this,args = arguments;
    clearTimeout(timeout);
    timeout = setTimeout(function(){
    fn.apply(ctx,args);
  },wait);
  };
};

