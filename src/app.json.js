export default {
  "permission": {
    "scope.userInfo": {
      "desc": "小程序将使用用户信息"
    },
    "scope.userLocation": {
      "desc": "小程序将获取地理位置"
    },
    "scope.userLocationBackground": {
      "desc": "小程序将使用后台定位"
    },
    "scope.address": {
      "desc": "小程序将使用通讯地址"
    },
    "scope.invoiceTitle": {
      "desc": "小程序将使用发票抬头"
    },
    "scope.invoice": {
      "desc": "小程序将使用获取发票"
    },
    "scope.werun": {
      "desc": "小程序将使用微信运动步数"
    },
    "scope.record": {
      "desc": "小程序将使用录音功能"
    },
    "scope.writePhotosAlbum": {
      "desc": "小程序将使用保存到相册"
    },
    "scope.camerao": {
      "desc": "小程序将使用摄像头"
    }
  },
  "pages": [
    "page/tabBar/component/index",
    "page/tabBar/API/index"
  ],
  "tabBar": {
    "color": "#404040",
    "selectedColor": "#108ee9",
    "backgroundColor": "#F5F5F9",
    "list": [
      {
        "pagePath": "page/tabBar/component/index",
        "text": "组件",
        "iconPath": "image/icon_component.png",
        "selectedIconPath": "image/icon_component_HL.png"
      },
      {
        "pagePath": "page/tabBar/API/index",
        "text": "API",
        "iconPath": "image/icon_API.png",
        "selectedIconPath": "image/icon_API_HL.png"
      }
    ]
  },
  "window": {
    "backgroundColor": "#F5F5F9",
    "usingComponents": {}
  },
  "subpackages": [
    {
      "root": "page/API",
      "pages": [
        "events/events",
        "share/share",
        "action-sheet/action-sheet",
        "alert/alert",
        "animation/animation",
        "canvas/canvas",
        "card-pack/card-pack",
        "choose-city/choose-city",
        "favorite/favorite",
        "choose-location/choose-location",
        "confirm/confirm",
        "contact/contact",
        "date-picker/date-picker",
        "option-menu/option-menu",
        "download-file/download-file",
        "file/file",
        "ocr/ocr",
        "ocr-idcard-face/ocr-idcard-face",
        "ocr-business-card/ocr-business-card",
        "ocr-vehicle/ocr-vehicle",
        "ocr-driver-license/ocr-driver-license",
        "ocr-business-license/ocr-business-license",
        "ocr-bank-card/ocr-bank-card",
        "ocr-train-ticket/ocr-train-ticket",
        "ocr-passport/ocr-passport",
        "ocr-general/ocr-general",
        "ocr-vehicle-plate/ocr-vehicle-plate",
        "ocr-vin/ocr-vin",
        "get-auth-code/get-auth-code",
        "get-location/get-location",
        "get-network-type/get-network-type",
        "get-system-info/get-system-info",
        "get-server-time/get-server-time",
        "get-user-info/get-user-info",
        "get-image-info/get-image-info",
        "get-title-color/get-title-color",
        "image/image",
        "keyboard/keyboard",
        "loading/loading",
        "make-phone-call/make-phone-call",
        "memory-warning/memory-warning",
        "multi-level-select/multi-level-select",
        "options-select/options-select",
        "navigation-bar-loading/navigation-bar-loading",
        "navigator/navigator",
        "open-location/open-location",
        "pull-down-refresh/pull-down-refresh",
        "pay-sign-center/pay-sign-center",
        "request/request",
        "request-payment/request-payment",
        "scan-code/scan-code",
        "set-navigation-bar/set-navigation-bar",
        "show-auth-guide/show-auth-guide",
        "storage/storage",
        "toast/toast",
        "upload-file/upload-file",
        "vibrate/vibrate",
        "watch-shake/watch-shake",
        "clipboard/clipboard",
        "bluetooth/bluetooth",
        "rsa/rsa",
        "page-scroll-to/page-scroll-to",
        "websocket/websocket",
        "zm-credit-borrow/zm-credit-borrow",
        "create-selector-query/create-selector-query",
        "sdk-version/sdk-version",
        "user-capture-screen/user-capture-screen",
        "screen/screen",
        "compress-image/compress-image",
        "report-analytics/report-analytics",
        "text-risk-identification/text-risk-identification",
        "create-inner-audiocontext/create-inner-audiocontext",
        "get-background-audio-manager/get-background-audio-manager",
        "generate-image-from-code/generate-image-from-code"
      ]
    },
    {
      "root": "page/component",
      "pages": [
        "button/button",
        "canvas/canvas",
        "checkbox/checkbox",
        "form/form",
        "icon/icon",
        "image/image",
        "input/input",
        "label/label",
        "map/map",
        "navigator/navigate",
        "navigator/redirect",
        "navigator/reLaunch",
        "navigator/navigator",
        "picker/picker",
        "picker-view/picker-view",
        "progress/progress",
        "radio/radio",
        "scroll-view/scroll-view",
        "slider/slider",
        "swiper/swiper",
        "switch/switch",
        "text/text",
        "textarea/textarea",
        "view/view",
        "lifestyle/lifestyle",
        "contact-button/contact-button",
        "webview/webview",
        "cover-view/cover-view",
        "movable-view/movable-view",
        "video/video",
        "rich-text/rich-text"
      ]
    }
  ]
}