/* eslint-disable */
import my from '../../../../alipay2vue/my.js';
import macro from '../../../../alipay2vue/macro.js';
const {getApp,getCurrentPages} = macro;
let latitude;
let longitude;
function formatLocation(longitude,latitude){
  longitude = Number(longitude).toFixed(2);
  latitude = Number(latitude).toFixed(2);
  return {
      "longitude":longitude.toString().split('.'),
      "latitude":latitude.toString().split('.')
};
};
export default formatLocation;

