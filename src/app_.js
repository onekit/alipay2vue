/* eslint-disable */
import my from '../alipay2vue/my.js';
import macro from '../alipay2vue/macro.js';
const {getApp,getCurrentPages} = macro;
import App from '../alipay2vue/OnekitApp.js';
export default App({
    "onLaunch"(options){
      console.log('App Launch',options);
      console.log('getSystemInfoSync',my.getSystemInfoSync());
      console.log('SDKVersion',my.SDKVersion);
    },
    "onShow"(){
      console.log('App Show');
    },
    "onHide"(){
      console.log('App Hide');
    },
    "globalData":{
        "hasLogin":false
}
});

