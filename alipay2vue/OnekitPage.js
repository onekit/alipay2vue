import $ from "jquery"
import Vue from "vue"
import { OBJECT } from "oneutil"
import OneKit from "./OneKit"
export default function (PAGE_JSON, dataKeys, mys, my_object) {

  function _init_data (THIS) {
    let temp_data = my_object.data ? OBJECT.clone(my_object.data) : {}
    // const onekit_dataKeys = Object.keys(temp_data)
    for (const dataKey of dataKeys) {
      if( my_object[dataKey]){
        continue;
      }
      const keyPath = dataKey.split('.')
      var temp = temp_data
      for (var k = 0; k < keyPath.length; k++) {
        const key = keyPath[k]
        const key2 = keyPath[k + 1]
        if (temp[key] != null) {
          if (key2 == "length") {
            temp[key] = [];
            break
          } else {
            temp = temp[key]
          }
        } else {
          temp[key] = "";
        }

      }
      //  console.log(temp_data)
    }
    THIS.onekit_data = temp_data
    return temp_data
  }


  function initMembers (instance, isPrev) {
    if (my_object) {
      for (let key of Object.keys(my_object)) {
        let obj = my_object[key];
        switch (key) {
          case "data":
          case "onLoad":
          case "onUnload":
          case "onReady":
          case "onShow":
          case "onHide":
            break;
          case "onShareAppMessage":
            break;
          default:
            if (isPrev) {
              if (typeof (obj) === "function") {
                instance.methods[key] = obj;
              }
            } else {
              if (typeof (obj) !== "function") {
                //instance[key] = obj ? OBJECT.clone(obj) : obj
                instance[key] = obj
              }
            }
            break;
        }
      }
    }
  }
  let vue_object = {
    data () {
      return this.data || _init_data(this)
    },
    created () {
      window.CURRENT = this
      initMembers(this, false);
      for (const key of Object.keys(mys)) {
        this[key] = mys[key]
      }
      //1
      this.onekit_render_begin = new Date().getTime()
      window.onekit_performances.push({
        duration: this.onekit_render_begin - window.onekit_rout_begin,
        entryType: "navigation",
        name: "route",
        navigationStart: window.onekit_rout_begin,
        navigationType: "navigateTo",
        path: this.$route.query,
        startTime: this.onekit_render_begin
      })
    },
    destroyed () {
      if (my_object["onUnload"]) {
        my_object["onUnload"].call(this);
      }
    },
    mounted () {

      const APP_JSON = window.APP_JSON;
      const query = this.$route.query
      /////////////////////////////////////////

      $(function () {
        $("div").contextmenu(function () {
          const e = e || window.event;
          if (e && e.preventDefault) {
            e.preventDefault();
          } else {
            window.event.returnValue = false;
          }
          window.parent.postMessage(this.outerText, '*')
        })
      });
      //
      // top.postMessage('hello', '*');
      /////////////////////////////////////////
      /////////////////////////////////////////
      this.data = _init_data(this)
      /////////////
      this.onekit_script_begin = new Date().getTime()
      window.onekit_performances.push({
        duration: this.onekit_script_begin - this.onekit_render_begin,
        entryType: "render",
        name: "firstRender",
        path: this.$route.query,
        startTime: this.onekit_render_begin
      })
      if (my_object["onLoad"]) {
        this.eventChannelID = query.eventChannelID
        delete query["eventChannelID"]
        my_object["onLoad"].call(this, query);
      }

      //////////////
      let WINDOW_JSON = {
        navigationBarBackgroundColor: "#000000",
        navigationBarTextStyle: "white",
        navigationStyle: "default",
        backgroundColor: "#000000",
        backgroundTextStyle: "dark",
      };
      if (APP_JSON.window) {
        for (let key of Object.keys(APP_JSON.window)) {
          let value = APP_JSON.window[key];
          if (value) {
            WINDOW_JSON[key] = typeof (value) == 'string' && value.startsWith("@") ? OneKit.theme()[value.substr(1)] : value;
          }
        }
      }
      if (typeof (PAGE_JSON) != "undefined") {
        for (let key in PAGE_JSON) {
          let item = PAGE_JSON[key];
          if (!item) {
            continue;
          }
          switch (key) {
            case "backgroundColorTop":
            case "backgroundColorBottom":
            case "enablePullDownRefresh":
            case "onReachBottomDistance":
            case "pageOrientation":
            case "navigationBarTitleText":
              WINDOW_JSON[key] = item
              break;
            case "usingComponents":

              break;
            default:
              throw new Error(key, item)
          }
        }
      }
      this.$emit('updatewindowjson', { WINDOW_JSON });

      if (my_object["onReady"]) {
        my_object["onReady"].call(this);
      }
    },
    beforeRouteEnter (to, from, next) {
      window.CURRENT = this
      next(vm => {
        //3
        if (my_object["onShow"]) {
          my_object["onShow"].call(vm);
        }
      });
    },
    beforeRouteLeave (to, from, next) {
      next(vm => {
        if (my_object["onHide"]) {
          my_object["onHide"].call(vm);
        }
      });
    },
    methods: {
      onekit_handle (name) {
        this[name]();
      },
      animate () {

      },
      setData (data) {
        if (data == null) {
          return
        }
        Object.assign(this.onekit_data, data)
        this.data = this.onekit_data
        this.$nextTick(function () {
          for (let k of Object.keys(data)) {
            const d = data[k]
            this[k] = d
          }
        });
      },
      getOpenerEventChannel () {
        return window.eventChannels[this.eventChannelID];
      }
    },
    components: {}

  };
  initMembers(vue_object, true);
  return vue_object;
}
