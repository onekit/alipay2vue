import {PATH} from 'oneutil'
export default class OneKit {

  static current () {
    return window.CURRENT;
  }
  static theme () {
    return window.THEMES_JSON.light;
  }
  static fixurl (url) {
    const APP_JSON = window.APP_JSON;
    const my_rel_url = url.includes("?") ? url.substring(0, url.indexOf("?")) : url
    const my_abs_url = PATH.rel2abs(this.currentUrl(), my_rel_url)
    let isExist
    isExist = APP_JSON.pages.includes(my_abs_url)
    if (!isExist && APP_JSON.subpackages) {
      for (const subpackage of APP_JSON.subpackages) {
        for (const page of subpackage.pages) {
          if (my_abs_url === `${subpackage.root}/${page}`) {
            isExist = true
            break
          }
        }
        if (isExist) {
          break
        }
      }
    }
    if (!isExist) {
      if (window.onPageNotFound) {
        window.onPageNotFound();
      }
      return null
    }
    const vue_path = my_abs_url // ...
    return vue_path
  }


  static currentUrl () {
    return this.current().$route.path;
  }


  //rgb转16进制
  static colorRGB2Hex (color) {
    const rgb = color.split(',')
    const r = parseInt(rgb[0])
    const g = parseInt(rgb[1])
    const b = parseInt(rgb[2])

    const hex = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)
    return hex
  }

  //16进制转rgb
  static Hex2colorRGB (color) {
    if (!color) return
    color = color.substring(1)
    color = color.toLowerCase()
    let b = new Array()
    for (let x = 0; x < 3; x++) {
      b[0] = color.substr(x * 2, 2)
      b[3] = "0123456789abcdef"
      b[1] = b[0].substr(0, 1)
      b[2] = b[0].substr(1, 1)
      b[20 + x] = b[3].indexOf(b[1]) * 16 + b[3].indexOf(b[2])
    }
    return "rgb(" + b[20] + "," + b[21] + "," + b[22] + ")"
  }
}
