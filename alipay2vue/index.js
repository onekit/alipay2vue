import ui from './ui'
import pages from './pages'
import App from './OnekitApp'
import Behavior from './OnekitBehavior'
import Component from './OnekitComponent'
import Page from './OnekitPage'
import my from './my'
import OneKit from "./OneKit"
import macro from './macro'
export default {
  install (Vue) {
    Vue.use(ui)
  },
  pages,
  App,
  Behavior,
  Component,
  Page,
  my,
  OneKit,
  macro
}
