/* eslint-disable vue/custom-event-name-casing */
export default {
  props: {
    animation: {
      type: Object
    }
  },
  methods: {

    ui_mousedown(e) {
      this.$emit("Touchstart", e);
    },
    ui_mousemove(e) {
      this.$emit("Touchmove", e);
    },
    ui_mouseup(e) {
      this.$emit("Touchend", e);
    },
    ui_touchstart(e) {
      this.$emit('Touchstart', {
        target: {
          dataset: e.target.dataset
        },
        stopPropagation() {
          e.preventDefault()
        }
      })
    },
    ui_touchmove(e) {
      this.$emit('Touchmove', {
        target: {
          dataset: e.target.dataset
        },
        stopPropagation() {
          e.preventDefault()
        }
      })
    },
    ui_touchend(e) {
      this.$emit('Touchend', {
        target: {
          dataset: e.target.dataset
        },
        stopPropagation() {
          e.preventDefault()
        }
      })
    },
    ui_click(e) {
      //console.log("bbbbb",e)
      this.$emit("Tap", e);
    },
    /* ui_longPress() {
       this.$emit("Longpress");
     },*/
    ui_longclick(e,) {
      this.$emit("Longtap", e);
    },
    animation_(animation) {
      const node = this.$el
      if (animation && animation.actions) {
        let delay = 0;
        for (const action of animation.actions) {
          let keyframe = {}
          var transform = "";
          for (const animation of action.animates) {
            const type = animation.type;
            const args = animation.args;
            switch (type) {
              case 'translateX':
              case 'translateY':
              case 'translateZ':
                transform += ` ${type}(${args[0]}px)`
                break;
              case 'rotate':
              case 'rotateX':
              case 'rotateY':
              case 'rotateZ':
                transform += ` ${type}(${args[0]}deg)`
                break;
              case 'scaleX':
              case 'scaleY':
              case 'scaleZ':
                transform += ` ${type}(${args[0]})`
                break;
              case 'perspective':
                transform += ` ${type}(${args[0]})`
                break;
              case 'skewX':
              case 'skewY':
                transform += ` ${type}(${args[0]}deg)`
                break;
              case 'translate':
                transform += ` ${type}(${args[0]}px, ${args[1]}px)`
                break;
              case 'skew':
                transform += ` ${type}(${args[0]}deg, ${args[1]}deg)`
                break;
              case 'translate3d':
                transform += ` ${type}(${args[0]}px, ${args[1]}px, ${args[2]}px)`
                break;
              case 'scale3d':
                transform += ` ${type}(${args[0]}, ${args[1]}, ${args[2]})`
                break;
              case 'rotate3d':
                transform += ` ${type}(${args[0]}, ${args[1]}, ${args[2]}, ${args[3]}deg)`
                break;
              case 'matrix':
                transform += ` ${type}(${args[0]}, ${args[1]}, ${args[2]}, ${args[3]}, ${args[4]}, ${args[5]})`
                break;
              case 'matrix3d':
                transform += ` ${type}(`
                  + `${args[0]}, ${args[1]}, ${args[2]}, ${args[3]},`
                  + `${args[4]}, ${args[5]}, ${args[6]}, ${args[7]},`
                  + `${args[8]}, ${args[9]}, ${args[10]}, ${args[11]},`
                  + `${args[12]}, ${args[13]}, ${args[14]}, ${args[15]}`
                  + `)`
                break;
              case 'scale':
                if (args.length == 2) {
                  transform += ` ${type}(${args[0]}, ${args[1]})`
                } else {
                  transform += ` ${type}(${args[0]})`
                }
                break;
              case 'style': {
                const style = args[0]
                switch (style) {
                  case 'background-color':
                    keyframe['backgroundColor'] = args[1];
                    break;
                  case "width":
                  case "height":
                  case "top":
                  case "bottom":
                  case "left":
                  case "right":
                  case "opacity":

                    keyframe[args[0]] = `${args[1]}`; break;
                  default:
                    throw style
                }
              }
                break;

              default:
                throw type
            }
          }
          if (transform) {
            keyframe.transform = transform;
          }
          console.log(keyframe)
          node.animate([keyframe], {
            delay: delay + action.option.transition.delay,
            duration: action.option.transition.duration,
            easing: action.option.transition.timingFunction,
            fill: 'forwards'
          });

          delay += action.option.transition.duration;
        }
      }
    }

  },
  watch: {
    animation(animation) {
      this.animation_(animation)
    }
  },
  mounted() {
    this.animation_(this.animation)
  }
}

