import uc from './share-element.vue'
uc.install = (Vue) => Vue.component(uc.name, uc);
export default uc
