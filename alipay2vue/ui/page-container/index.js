import uc from './page-container.vue'
uc.install = (Vue) => Vue.component(uc.name, uc);
export default uc
