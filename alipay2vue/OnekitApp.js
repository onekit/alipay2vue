import WebFSO from 'alipay2html/js/WebFSO';
export default function (APP) {
  window.onekit_nodes = {}
  window.onekit_performances = [];
  window.onekit_rout_begin = new Date().getTime()
  window.APP = APP
  // console.log("confirm", localStorage.getItem("onekit_fso"))
  if (localStorage.getItem("onekit_fso")) {
    const c = confirm("是否加载文件")
    if (c) {
      WebFSO.load()
    }
  }
  //
  setTimeout(() => {
    window.onekit_performances.push({
      entryType: "navigation",
      name: "appLaunch",
      navigationType: "appLaunch",
      path: window.location.pathname.substr(1)
    })
  }, 1000)

  if (APP.onLaunch) {
    APP.onLaunch(window.OPTION);
  }
  if (APP.onShow) {
    APP.onShow(window.OPTION);
  }
}
