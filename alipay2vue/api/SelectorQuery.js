import OneKit from "../OneKit"
import NodeRef from "./NodeRef"
export default class SelectorQuery {
  constructor(my) {
    this.my = my
    this._component = null
    this._defaultComponent = OneKit.current()
    this._queue = []
    this._queueCb = []
  }
  exec(callback) {
    const results = [];

    for (let i = 0, index = 0; i < this._queue.length; i++) {
      const item = this._queue[i]
      let nodes
      if (item.component === 0) {
        nodes = [document.body]
      } else {
        const parent = item.component || document
        nodes = item.single ?
          [parent.querySelector(item.selector)] :
          parent.querySelectorAll(item.selector)
      }
      const resultList = []
      for (const node of nodes) {
        if (!node) {
          resultList.push(null)
          continue;
        }
        const result = {}
        for (const field of Object.keys(item.fields)) {
          if (!item.fields[field]) {
            continue
          }
          const rect = node.getBoundingClientRect()
          switch (field) {
            case "id":
              result.id = node.id;
              break
            case "dataset":
              result.dataset = node.dataset;
              break
            case "rect":
              result.left = rect.left
              result.right = rect.right
              result.top = rect.top - 40
              result.bottom = rect.bottom - 40
              break
            case "size":
              result.width = rect.width
              result.height = rect.height
              break
            case "node":
              // result.node =  window.onekit_nodes[`_${node.id}`];
              result.node = node
              break
            case "context":
              switch (node.className.split(" ").filter((name) => {
                return name.startsWith("onekit-")
              })[0]) {
                case "onekit-video":
                  result.context = this.my.createVideoContext(node.id)
                  break
                case "onekit-canvas":
                  result.context = this.my.createCanvasContext(node.id)
                  break
                case "onekit-liveplayer":
                  result.context = this.my.createLivePlayerContext(node.id)
                  break
                case "onekit-editor":
                  result.context = this.my.createEditorContext(node.id)
                  break
                case "onekit-map":
                  result.context = this.my.createMapContext(node.id)
                  break
                default:
                  throw new Error(node.className)
              }
              break
            case "scrollOffset":
              result.scrollTop = node.scrollTop
              result.scrollLeft = node.scrollLeft
              result.scrollWidth = node.scrollWidth
              result.scrollHeight = node.scrollHeight
              break
            default:
              throw new Error(field)
          }
        }
        resultList.push(result)
        if (this._queueCb[index]) {
          this._queueCb[index](result)
        }
      }
      results.push(item.single?resultList[0]:resultList)
    }
    if (callback && this._queue.length > 0) {
      callback(results)
    }
  } in(component) {
    this._component = component
    return this
  }
  select(selector) {
    return new NodeRef(this, selector, true, this._component)
  }
  selectAll(selector) {
    return new NodeRef(this, selector, false, this._component)
  }
  selectViewport() {
    return new NodeRef(this, "", true, 0)
  }
}
