import EventBus from './EventBus'
export default class EventChannel {
  emit(eventName, args) {
    EventBus.$emit(eventName, args)
  }

  off(eventName, callback) {
    EventBus.$off(eventName, callback)
  }

  on(eventName, callback) {
    EventBus.$on(eventName, callback)
  }

  once(eventName, callback) {
    EventBus.$once(eventName, callback)
  }
}
