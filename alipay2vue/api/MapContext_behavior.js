import { PROMISE } from 'oneutil'
import MobileDetect from 'mobile-detect'
import my from '../my'
let markerClusters;
let src;
let web_sw;
let web_ne;
export default {
  methods: {
    addCustomLayer_ (my_object) {
      if (!my_object) {
        return
      }
      const my_layerId = my_object.layerId
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null

      PROMISE((SUCCESS, FAIL) => {
        if (!my_layerId) {
          FAIL({
            errMsg: 'addGroundOverlay:err'
          })
          return
        }
        new TMap.ImageTileLayer.createCustomLayer({
          layerId: my_layerId,
          map: this.map,
        }).then(customLayer => {
          if (customLayer) {
            const my_res = {
              errMsg: 'addGroundOverlay:ok'
            }
            SUCCESS(my_res)
          } else {
            FAIL({
              errMsg: 'addGroundOverlay:err'
            })
          }
        })
      }, my_success, my_fail, my_complete)
    },

    addGroundOverlay_ (my_object) {
      if (!my_object) {
        return
      }
      const my_id = my_object.id
      const my_src = my_object.src
      const my_bounds = my_object.bounds
      const my_visible = my_object.visible
      const my_zIndex = my_object.zIndex
      const my_opacity = my_object.opacity
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      //
      src = my_src
      web_sw = new TMap.LatLng(my_bounds.southwest.latitude, my_bounds.southwest.longitude)
      web_ne = new TMap.LatLng(my_bounds.northeast.latitude, my_bounds.northeast.longitude)
      PROMISE((SUCCESS, FAIL) => {
        if (!my_id || !my_src || !my_bounds) {
          FAIL({
            errMsg: 'addGroundOverlay:err'
          })
          return
        }
        new TMap.ImageGroundLayer({
          bounds: new TMap.LatLngBounds(web_sw, web_ne),
          src: my_src,
          map: this.map,
          visible: my_visible || true,
          zIndex: my_zIndex || 1,
          opacity: my_opacity || 1,
        })
        const my_res = {
          errMsg: 'addGroundOverlay:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    addMarkers_ (my_object) {
      if (!my_object) {
        return
      }
      const my_markers = my_object.markers
      markerClusters = my_markers
      const my_clear = my_object.clear
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (!my_markers) {
          FAIL({
            errMsg: 'addMarkers:err'
          })
          return
        }
        this.markers_(my_markers, my_clear)
        const my_res = {
          errMsg: 'addMarkers:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    fromScreenLocation_ (my_object) {
      if (!my_object) {
        return
      }
      const my_x = my_object.x
      const my_y = my_object.y
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (my_x === null || !my_y === null) {
          FAIL({
            errMsg: 'fromScreenLocation:err'
          })
          return
        }
        const my_res = this.map.unprojectFromContainer(new TMap.Point(my_x, my_y))
        my_res.errMsg = 'fromScreenLocation:ok'
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    getCenterLocation_ (my_object) {
      if (!my_object) {
        return
      }
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS) => {
        const my_res = this.map.getCenter()
        my_res.errMsg = 'getCenterLocation:ok'
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    getRegion_ (my_object) {
      if (!my_object) {
        return
      }
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS) => {
        const web_res = this.map.getBounds()
        const my_res = {
          northeast: {
            latitude: web_res._ne.lat,
            longitude: web_res._ne.lng
          },
          southwest: {
            latitude: web_res._sw.lat,
            longitude: web_res._sw.lng
          },
          errMsg: 'getRegion:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    getRotate_ (my_object) {
      if (!my_object) {
        return
      }
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS) => {
        const web_res = this.map.getRotation()
        const my_res = {
          rotate: web_res,
          errMsg: 'getRotate:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    getScale_ (my_object) {
      if (!my_object) {
        return
      }
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS) => {
        const web_res = this.map.getZoom()
        const my_res = {
          scale: web_res,
          errMsg: 'getScale:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    getSkew_ (my_object) {
      if (!my_object) {
        return
      }
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS) => {
        const web_res = this.map.getPitch()
        const my_res = {
          skew: web_res,
          errMsg: 'getScale:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    includePoints_ (my_object) {
      if (!my_object) {
        return
      }
      const my_points = my_object.points
      const my_padding = my_object.padding
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (!my_points) {
          FAIL({
            errMsg: 'includePoints:err'
          })
          return
        }
        this.includePoints_(my_points, my_padding)
        const my_res = {
          errMsg: 'includePoints:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    initMarkerCluster_ (my_object) {
      if (!my_object) {
        return
      }
      const my_enableDefaultStyle = my_object.enableDefaultStyle
      const my_zoomOnClick = my_object.zoomOnClick
      const my_gridSize = my_object.gridSize
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS) => {
        new TMap.MarkerCluster({
          map: this.map,
          enableDefaultStyle: my_enableDefaultStyle || true,
          zoomOnClick: my_zoomOnClick || true,
          gridSize: my_gridSize || 60,
          geometries: [{
            position: new TMap.LatLng(this.latitude, this.longitude)
          }],
        })
        const my_res = {
          errMsg: 'initMarkerCluster:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    moveAlong_ (my_object) {
      //编程没问题 测试有问题
      if (!my_object) {
        return
      }
      const my_markerId = my_object.markerId
      const my_path = my_object.path
      const my_autoRotate = my_object.autoRotate
      const my_duration = my_object.duration
      const my_success = my_object.my_success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (!my_markerId || !my_path || !my_duration) {
          FAIL({
            errMsg: 'moveAlong:err'
          })
          return
        }
        const web_path = my_path.map(path => {
          return new TMap.LatLng(path.latitude, path.longitude)
        })
        const object = {}
        object[my_markerId] = {
          path: web_path,
          duration: my_duration,
        }
        const web_MultiMarker = this.marker_(my_markerId)
        // console.log(web_MultiMarker, object)
        web_MultiMarker.moveAlong(object, {
          autoRotation: my_autoRotate || true
        })
        const my_res = {
          errMsg: 'moveAlong:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    moveToLocation_ (my_object) {
      if (!my_object) {
        return
      }
      const my_longitude = my_object.longitude
      const my_latitude = my_object.latitude
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS) => {
        if (!this.showLocation) {
          return
        }
        if (my_longitude && my_latitude) {
          this.map.panTo(new TMap.LatLng(my_longitude, my_latitude))
        } else {
          navigator.geolocation.getCurrentPosition((position) => {
            this.map.panTo(new TMap.LatLng(position.coords.longitude, position.coords.latitude))
          })
        }
        const my_res = {
          errMsg: 'moveToLocation:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    on_ (eventName, handlers) {
      if (!eventName && !handlers) {
        return
      }
      const my_eventName = eventName
      const my_handlers = handlers
      const my_success = handlers.success
      const my_fail = handlers.fail
      const my_complete = handlers.complete
      eventName = null
      handlers = null
      PROMISE((SUCCESS) => {
        console.log(markerClusters)
        let web_geometries = markerClusters.map((markerCluster) => {
          if (markerCluster.joinCluster) {
            return {
              id: markerCluster.id,
              position: new TMap.LatLng(markerCluster.latitude, markerCluster.longitude)
            }
          }
        })
        const markerCluster = new TMap.MarkerCluster({
          id: 'cluster',
          map: this.map,
          enableDefaultStyle: true, // 启用默认样式
          minimumClusterSize: 2, // 形成聚合簇的最小个数
          geometries: web_geometries,
          zoomOnClick: true, // 点击簇时放大至簇内点分离
          gridSize: 60, // 聚合算法的可聚合距离
          averageCenter: false, // 每个聚和簇的中心是否应该是聚类中所有标记的平均值
          maxZoom: 10 // 采用聚合策略的最大缩放级别
        })
        const my_res = markerCluster.on(my_eventName, my_handlers)
        my_res.errMsg = 'on:ok'
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    openMapApp_ (my_object) {
      if (!my_object) {
        return
      }
      const my_longitude = my_object.longitude
      const my_latitude = my_object.latitude
      const my_destination = my_object.destination
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (!my_longitude || !my_latitude || !my_destination) {
          FAIL({
            errMsg: 'addGroundOverlay:err'
          })
          return
        }
        if (!this.showLocation) {
          return
        }
        navigator.geolocation.getCurrentPosition((position) => {
          const initposition = position.coords
          const md = new MobileDetect(navigator.userAgent)
          const os = md.os()
          let url
          switch (os) {
            case 'iOS':
              my.showActionSheet({
                itemList: ['腾讯地图', '高德地图', '百度地图'],
                success (res) {
                  console.log(res.tapIndex)
                  switch (res.tapIndex) {
                    case 0:
                      url = 'qqmap://map/routeplan?type=drive&fromcoord=' + initposition.latitude + ',' + initposition.longitude + '&to=' + my_destination + '&tocoord=' + my_latitude + ',' + my_longitude + '&referer=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77';
                      break;
                    case 1:
                      url = 'iosamap://navi?sourceApplication=amap&poiname=' + my_destination + '&lat=' + my_latitude + '&lon=' + my_longitude + '&dev=1&style=0'
                      break;
                    case 2:
                      url = 'baidumap://map/navi?location=' + my_latitude + ',' + my_longitude + '&src=ios.baidu.openAPIdemo&coord_type=gcj02'
                      break;
                    default:
                      throw new Error(res.tapIndex);
                  }
                  console.log(res.tapIndex, url)
                  window.open(url)
                },
                fail (res) {
                  console.log(res.errMsg)
                }
              })
              break;
            case 'AndroidOS':
              my.showActionSheet({
                itemList: ['腾讯地图', '高德地图', '百度地图'],
                success (res) {
                  console.log(res.tapIndex)
                  switch (res.tapIndex) {
                    case 0:
                      url = 'qqmap://map/routeplan?type=drive&fromcoord=' + initposition.latitude + ',' + initposition.longitude + '&to=' + my_destination + '&tocoord=' + my_latitude + ',' + my_longitude + '&referer=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77';
                      break;
                    case 1:
                      url = 'androidamap://navi?sourceApplication=amap&poiname=' + my_destination + '&lat=' + my_latitude + '&lon=' + my_longitude + '&dev=1&style=0'
                      break;
                    case 2:
                      url = 'baidumap://map/navi?location=' + my_latitude + ',' + my_longitude + '&src=andr.baidu.openAPIdemo&coord_type=gcj02'
                      break;
                    default:
                      throw new Error(res.tapIndex);
                  }
                  window.open(url)
                },
                fail (res) {
                  console.log(res.errMsg)
                }
              })
              break;
            default:
              my.showActionSheet({
                itemList: ['腾讯地图', '高德地图', '百度地图'],
                success (res) {
                  switch (parseInt(res.tapIndex, 10)) {
                    case 0:
                      url = 'https://apis.map.qq.com/uri/v1/routeplan?type=drive&fromcoord=' + initposition.latitude + ',' + initposition.longitude + '&to=' + my_destination + '&tocoord=' + my_latitude + ',' + my_longitude + '&policy=0&referer=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77';
                      break;
                    case 1:
                      url = 'https://uri.amap.com/navigation?from=' + initposition.longitude + ',' + initposition.latitude + ',startpoint&to=' + my_longitude + ',' + my_latitude + ',endpoint&coordinate=gaode'
                      break;
                    case 2:
                      url = 'http://api.map.baidu.com/direction?origin=latlng:' + initposition.latitude + ',' + initposition.longitude + '&destination=latlng:' + my_latitude + ',' + my_longitude + '&mode=driving&output=html&coord_type=gcj02&src=webapp.baidu.openAPIdemo'
                      break;
                    default:
                      throw new Error(res.tapIndex);
                  }
                  window.open(url)
                },
                fail (res) {
                  console.log(res.errMsg)
                }
              })
              break;
          }
          const my_res = {
            errMsg: 'openMapApp:ok'
          }
          SUCCESS(my_res)
        });
        /*
        const geolocation = new qq.maps.Geolocation();
        const initposition = geolocation.getLocation((position) => {
          console.log(position)
          return position
        }, () => {
          console.error("定位失败");
        });
        */
        //1.

      }, my_success, my_fail, my_complete)
    },

    removeCustomLayer_ (my_object) {
      if (!my_object) {
        return
      }
      const my_layerId = my_object.layerId
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null

      PROMISE((SUCCESS, FAIL) => {
        if (!my_layerId) {
          FAIL({
            errMsg: 'removeCustomLayer:err'
          })
          return
        }
        new TMap.ImageTileLayer.createCustomLayer({
          layerId: my_layerId,
          map: this.map,
          visible: false,
          opacity: 0,
        }).then(customLayer => {
          if (customLayer) {
            const my_res = {
              errMsg: 'removeCustomLayer:ok'
            }
            SUCCESS(my_res)
          } else {
            FAIL({
              errMsg: 'removeGroundOverlay:err'
            })
          }
        })
      }, my_success, my_fail, my_complete)
    },

    removeGroundOverlay_ (my_object) {
      if (!my_object) {
        return
      }
      const my_id = my_object.id
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (!my_id) {
          FAIL({
            errMsg: 'removeGroundOverlay:err'
          })
          return
        }
        new TMap.ImageGroundLayer({
          bounds: new TMap.LatLngBounds(web_sw, web_ne),
          src,
          map: this.map,
          visible: false,
          opacity: 0,
        })
        const my_res = {
          errMsg: 'removeGroundOverlay:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    removeMarkers_ (my_object) {
      if (!my_object) {
        return
      }
      const my_markerIds = my_object.markerIds
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (!my_markerIds) {
          FAIL({
            errMsg: 'removeMarkers:err'
          })
          return
        }
        my_markerIds.forEach(my_markerId => {
          this.marker_(my_markerId)
        })
        const my_res = {
          errMsg: 'removeMarkers:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    setCenterOffset_ (my_object) {
      if (!my_object) {
        return
      }
      const my_offset = my_object.offset
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (!my_offset) {
          FAIL({
            errMsg: 'setCenterOffset:err'
          })
          return
        }
        this.map.setOffset({ x: my_offset[0] * 320, y: my_offset[1] * 400 })
        const my_res = {
          errMsg: 'setCenterOffset:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    toScreenLocation_ (my_object) {
      if (!my_object) {
        return
      }
      const my_latitude = my_object.latitude
      const my_longitude = my_object.longitude
      const my_success = my_object.success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (!my_latitude || !my_longitude) {
          FAIL({
            errMsg: 'toScreenLocation:err'
          })
          return
        }
        const my_res = this.map.projectToContainer(new TMap.LatLng(my_latitude, my_longitude))
        my_res.errMsg = 'toScreenLocation:ok'
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    translateMarker_ (my_object) {
      //编程没问题 测试有问题
      if (!my_object) {
        return
      }
      const my_markerId = my_object.markerId
      const my_destination = my_object.destination
      const my_autoRotate = my_object.autoRotate
      const my_rotate = my_object.rotate
      const my_duration = my_object.duration
      const my_animationEnd = my_object.animationEnd
      const my_success = my_object.my_success
      const my_fail = my_object.fail
      const my_complete = my_object.complete
      my_object = null
      PROMISE((SUCCESS, FAIL) => {
        if (!my_markerId || !my_destination || !my_autoRotate || !my_rotate) {
          FAIL({
            errMsg: 'moveAlong:err'
          })
          return
        }
        const web_destination = new TMap.LatLng(my_destination.latitude, my_destination.longitude)
        const object = {}
        object[my_markerId] = {
          path: web_destination,
          duration: my_duration,
        }
        const web_MultiMarker = this.marker_(my_markerId)
        // console.log(web_MultiMarker, object)
        web_MultiMarker.moveAlong(object, {
          autoRotation: my_autoRotate || true,
          rotate: my_rotate || 0,
        })
        web_MultiMarker.on('move_ended', my_animationEnd)
        const my_res = {
          errMsg: 'moveAlong:ok'
        }
        SUCCESS(my_res)
      }, my_success, my_fail, my_complete)
    },

    updateGroundOverlay_ (my_object) {
      this.addGroundOverlay_(my_object)
    }
  }
}
