export default class getPerformance {

  constructor() { }

  createObserver(web_callback) {
    return web_callback
  }

  getEntries() {
    return window.onekit_performances
  }

  getEntriesByName(name, entryType) {
    for (let i = 0; i < window.onekit_performances.length; i++) {
      if (window.onekit_performances[i].name === name && window.onekit_performances[i].entryType === entryType || window.onekit_performances[i].name === name) {
        return window.onekit_performances[i]
      }
    }
  }

  getEntriesByType(entryType) {
    for (let i = 0; i < window.onekit_performances.length; i++) {
      if (window.onekit_performances[i].entryType === entryType) {
        return window.onekit_performances[i]
      }
    }
  }

  setBufferSize(num) {
    const defaults = 30
    if (num) {
      window.onekit_performances.length = num
      return
    }
    window.onekit_performances.length = defaults
  }


}