import { PROMISE } from 'oneutil'
export default class CameraFrameListener {
  constructor() { }
  start (my_object) {
    const { success, fail, complete } = my_object

    PROMISE(SUCCESS => {
      SUCCESS()
    }, success, fail, complete)
  }
  stop (my_object) {
    const { success, fail, complete } = my_object
    PROMISE(SUCCESS => {
      SUCCESS()
    }, success, fail, complete)
  }
}
