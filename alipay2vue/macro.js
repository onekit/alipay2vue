const Any = null
class global{
   static get isDemo(){
    return global._isDemo;
  }
  static set isDemo(isDemo){
     global._isDemo = isDemo;
  }
}
function getApp() {
  return window.APP
}

function getCurrentPages() {
  return  window.CURRENT.$route;
}
export default { global,Any, getApp, getCurrentPages }
