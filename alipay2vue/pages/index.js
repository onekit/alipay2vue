import activity from "./activity"
import openLocation from "./openLocation"
import openSetting from "./openSetting"
import detailSetting from "./detailSetting"
import previewMedia from "./previewMedia"
import tabs from "./tabs"
export default {
  activity,
  openLocation,
  previewMedia,
  tabs,
  openSetting,
  detailSetting
}
