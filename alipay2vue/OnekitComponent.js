import $ from "jquery"
import Vue from "vue"
import { OBJECT } from "oneutil"
import OneKit from "../alipay2vue/OneKit"
export default function (PAGE_JSON, dataKeys, mys, my_object) {

  function _init_data (THIS) {
    let temp_data = my_object.data ? OBJECT.clone(my_object.data) : {}
    // const onekit_dataKeys = Object.keys(temp_data)
    for (const dataKey of dataKeys) {
      const keyPath = dataKey.split('.')
      // console.log(keyPath)
      var temp = temp_data
      for (var k = 0; k < keyPath.length; k++) {
        const key = keyPath[k]
        const key2 = keyPath[k + 1]
        if (temp[key] != null) {
          if (key2 == "length") {
            temp[key] = [];
            break
          } else {
            temp = temp[key]
          }
        } else {
          temp[key] = "";
        }

      }
      //  console.log(temp_data)
    }

    THIS.onekit_data = temp_data
    return temp_data
  }


  function initMembers (instance, isPrev) {
    if (my_object) {
      for (let key of Object.keys(my_object)) {
        let obj = my_object[key];
        switch (key) {
          case "data":
          case "onLoad":
          case "onUnload":
          case "onReady":
          case "onShow":
          case "onHide":
            break;
          case "onShareAppMessage":
            break;
          default:
            if (isPrev) {
              if (typeof (obj) === "function") {
                instance.methods[key] = obj;
              }
            } else {
              if (typeof (obj) !== "function") {
                //instance[key] = obj ? OBJECT.clone(obj) : obj
                instance[key] = obj
              }
            }
            break;
        }
      }
    }
  }
  let vue_object = {
    data () {
      const data = this.data || _init_data(this)


      return data;
    },
    created () {
      window.CURRENT = this
      initMembers(this, false);
      for (const key of Object.keys(mys)) {
        this[key] = mys[key]
      }
    },
    destroyed () {
      if (my_object["detached"]) {
        my_object["detached"].call(this);
      }
    },
    mounted () {

      /////////////////////////////////////////

      $(function () {
        $("div").contextmenu(function () {
          const e = e || window.event;
          if (e && e.preventDefault) {
            e.preventDefault();
          } else {
            window.event.returnValue = false;
          }
          window.parent.postMessage(this.outerText, '*')
        })
      });
      //
      /////////////////////////////////////////
      /////////////////////////////////////////
      this.data = _init_data(this)
      if (my_object.properties) {
        for (let propertyName of Object.keys(my_object.properties)) {
          this.data[propertyName] = this[propertyName]
        }
      }


      //////////////


      if (my_object["attached"]) {
        my_object["attached"].call(this);
      }
    },
    methods: {
      onekit_pro () {

      },
      onekit_handle (name) {
        this[name]();
      },
      animate () {

      },
      setData (data) {
        if (data == null) {
          return
        }
        Object.assign(this.onekit_data, data)
        this.data = this.onekit_data
        this.$nextTick(function () {
          for (let k of Object.keys(data)) {
            const d = data[k]
            this[k] = d
          }
        });
      }
    },
    components: {}

  };
  if (!vue_object.watch) {
    vue_object.watch = {};
  }
  if (my_object.properties) {
    vue_object.props = {};
    for (let propertyName of Object.keys(my_object.properties)) {
      const property = my_object.properties[propertyName]
      if (!property) {
        continue;
      }
      let prop
      switch (typeof (property)) {
        case "object":
          prop = {}
          if (property.type) {
            prop.type = property.type
          }
          if (property.value) {
            prop.default = property.value
          }
          break;
        case "function":
          prop = { type: property }
          break;
        default:
          throw new Error(property);
      }
      vue_object.props[propertyName] = prop;
      vue_object.watch[propertyName] = {
        deep: true,
        immediate: true,
        handler (v) {
          //console.log("watch",propertyName,v)
          //  this.onekit_data[propertyName] = v;
          // this.data = this.onekit_data;
          // this.$nextTick(function () { this[propertyName] = v; });
        }
      }
    }
  }
  initMembers(vue_object, true);
  return vue_object;
}
