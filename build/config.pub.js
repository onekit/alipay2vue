const utils = require('./utils')

module.exports = {
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'alipay2vue': utils.resolve('alipay2vue/index.js'),
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
}